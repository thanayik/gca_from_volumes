#!/usr/bin/env bash

regions=("frontal_L_volmm3" "frontal_R_volmm3" "parietal_occipital_L_volmm3" "parietal_occipital_R_volmm3" "temporal_L_volmm3" "temporal_R_volmm3" "third_vent_volmm3" "v_frontal_horn_L_volmm3" "v_frontal_horn_R_volmm3" "v_occipital_horn_L_volmm3" "v_occipital_horn_R_volmm3" "v_temporal_horn_L_volmm3" "v_temporal_horn_R_volmm3" "brain_volume_volmm3")

for region in "${regions[@]}"
do
   fslpython main.py train --trainingcsv merged.csv --subset orchard --region $region
done