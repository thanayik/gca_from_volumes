import warnings
# make pandas not print the warning
warnings.filterwarnings("ignore", category=DeprecationWarning)
import argparse
import os
import numpy as np
import nibabel as nib
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression, BayesianRidge
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.neural_network import MLPRegressor
from sklearn import svm
from sklearn import metrics, model_selection
from sklearn import preprocessing
import seaborn as sns



regions = [
    'frontal_L',
    'frontal_R',
    'parietal_occipital_L',
    'parietal_occipital_R',
    'temporal_L',
    'temporal_R',
    'third_vent',
    'v_frontal_horn_L',
    'v_frontal_horn_R',
    'v_occipital_horn_L',
    'v_occipital_horn_R',
    'v_temporal_horn_L',
    'v_temporal_horn_R'
]
# add _volmm3 to each region
regions = [region + '_volmm3' for region in regions]

def scatter_predicted_vs_actual(y_test, y_pred, args):
    '''
    use sns to scatter plot the predicted vs actual values
    '''
    mae = metrics.mean_absolute_error(y_test, y_pred)
    sns.lmplot(x='x', y='y', fit_reg=True, data=pd.DataFrame({'x': y_test, 'y': y_pred}))
    plt.xlabel('Actual')
    plt.ylabel('Predicted')
    # add args.model to the title and the N = number of samples
    plt.title(f'{args.model} N={len(y_test)} MAE={mae:.2f}')
    # sex x axis to 0-39    
    plt.xlim(0, 39)
    # set y axis to 0-39
    plt.ylim(0, 39)
    # figure path is relative to this file and saved with args.model name
    plt.savefig(os.path.join(os.path.dirname(__file__), args.model + '.png'), dpi=300, bbox_inches='tight')

def do_mlp(X_train, y_train, X_test, y_test):
    # Create a linear regressor
    model = MLPRegressor(random_state=42, max_iter=5000)
    # Train the model
    model.fit(X_train, y_train)
    # Predict on the test set
    y_pred = model.predict(X_test)
    # Print out the mean absolute error (mae)
    print('MLP Mean Absolute Error: ', metrics.mean_absolute_error(y_test, y_pred))
    # Print out the mean squared error (mse)
    print('MLP Mean Squared Error: ', metrics.mean_squared_error(y_test, y_pred))
    return y_test, y_pred

def do_bayesian_ridge(X_train, y_train, X_test, y_test):
    # Create a linear regressor
    model = BayesianRidge()
    # Train the model
    model.fit(X_train, y_train)
    # Predict on the test set
    y_pred = model.predict(X_test)
    # Print out the mean absolute error (mae)
    print('Bayesian ridge Mean Absolute Error: ', metrics.mean_absolute_error(y_test, y_pred))
    # Print out the mean squared error (mse)
    print('Bayesian ridge Mean Squared Error: ', metrics.mean_squared_error(y_test, y_pred))
    return y_test, y_pred
    
def do_random_forest(X_train, y_train, X_test, y_test):
    # Create a random forest regressor
    model = RandomForestRegressor(random_state=42, n_estimators=1000)
    # Train the model
    model.fit(X_train, y_train)
    # Predict on the test set
    y_pred = model.predict(X_test)
    # Print out the mean absolute error (mae)
    print('Random forest Mean Absolute Error: ', metrics.mean_absolute_error(y_test, y_pred))
    # Print out the mean squared error (mse)
    print('Random forest Mean Squared Error: ', metrics.mean_squared_error(y_test, y_pred))
    feature_importances = pd.Series(model.feature_importances_, index=regions).sort_values(ascending=False)
    #  Plot a simple bar chart and save it
    feature_importances.plot(kind='bar', title='Feature Importances')
    plt.ylabel('Feature Importance Score')
    plt.savefig(os.path.join(os.path.dirname(__file__), 'random_forest_feature_importances.png'), dpi=300, bbox_inches='tight')
    return y_test, y_pred


def do_linear_regression(X_train, y_train, X_test, y_test):
    # Create a linear regressor
    model = LinearRegression()
    # Train the model
    model.fit(X_train, y_train)
    # Predict on the test set
    y_pred = model.predict(X_test)
    # Print out the mean absolute error (mae)
    print('Linear regression Mean Absolute Error: ', metrics.mean_absolute_error(y_test, y_pred))
    # Print out the mean squared error (mse)
    print('Linear regression Mean Squared Error: ', metrics.mean_squared_error(y_test, y_pred))
    # get the feature importances and save the plot
    feature_importances = pd.Series(np.abs(model.coef_), index=regions).sort_values(ascending=False)
    #  Plot a simple bar chart and save it
    feature_importances.plot(kind='bar', title='Feature Importances')
    plt.ylabel('Feature Importance Score')
    plt.savefig(os.path.join(os.path.dirname(__file__), 'linear_regression_feature_importances.png'), dpi=300, bbox_inches='tight')
    return y_test, y_pred

def do_svm(X_train, y_train, X_test, y_test):
    # Create a linear regressor
    model = svm.SVR()
    # Train the model
    model.fit(X_train, y_train)
    # Predict on the test set
    y_pred = model.predict(X_test)
    # Print out the mean absolute error (mae)
    print('SVM Mean Absolute Error: ', metrics.mean_absolute_error(y_test, y_pred))
    # Print out the mean squared error (mse)
    print('SVM Mean Squared Error: ', metrics.mean_squared_error(y_test, y_pred))
    return y_test, y_pred

def do_decision_tree(X_train, y_train, X_test, y_test):
    # Create a linear regressor
    model = DecisionTreeRegressor()
    # Train the model
    model.fit(X_train, y_train)
    # Predict on the test set
    y_pred = model.predict(X_test)
    # Print out the mean absolute error (mae)
    print('Decision tree Mean Absolute Error: ', metrics.mean_absolute_error(y_test, y_pred))
    # Print out the mean squared error (mse)
    print('Decision tree Mean Squared Error: ', metrics.mean_squared_error(y_test, y_pred))
    return y_test, y_pred

# Try multiple models to predic gca_total from the regional volmm3 csf volumes
def main(args):
    # Read in the csv file
    df = pd.read_csv(args.csv)
    # make the test train split
    X = df[regions]
    y = df['gca_total']
    X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.2, random_state=42)
    # scale the data to be between 0 and 1
    scaler = preprocessing.MinMaxScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)
    # print number of samples in each set
    print(f'X_train: {len(X_train)}')
    print(f'X_test: {len(X_test)}')
    # print number of samples in each set
    print(f'y_train: {len(y_train)}')
    print(f'y_test: {len(y_test)}')
    # print the number of features
    print(f'Number of features: {X_train.shape[1]}')

    if args.model == 'linear-regression':
        # do the linear regression
        y_test, y_pred = do_linear_regression(X_train, y_train, X_test, y_test)
    elif args.model == 'random-forest':
        # do the random forest
        y_test, y_pred = do_random_forest(X_train, y_train, X_test, y_test)
    elif args.model == 'svr':
        # do the svm
        y_test, y_pred = do_svm(X_train, y_train, X_test, y_test)
    elif args.model == 'decision-tree':
        # do the decision tree
        y_test, y_pred = do_decision_tree(X_train, y_train, X_test, y_test)
    elif args.model == 'bayesian-ridge':
        # do the bayesian ridge
        y_test, y_pred = do_bayesian_ridge(X_train, y_train, X_test, y_test)
    elif args.model == 'mlp':
        # do the mlp
        y_test, y_pred = do_mlp(X_train, y_train, X_test, y_test)
    else:
        print(f'Model {args.model} not available')
        return
    # scatter plot the predicted vs actual
    scatter_predicted_vs_actual(y_test, y_pred, args)



if __name__ == '__main__':
    # get arguments for the csv file and method (model)
    # example: fslpython prediction_models.py --csv merged.csv --model random-forest
    parser = argparse.ArgumentParser()
    parser.add_argument('--csv', type=str, help='path to csv file containing the data')
    parser.add_argument('--model', type=str, default='random-forest', help='model to use')
    args = parser.parse_args()
    main(args)