'''
train and infer GCA score from brain volume
Author: Taylor Hanayik
'''
import argparse
import os
import numpy as np
import nibabel as nib
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn import metrics, model_selection
import seaborn as sns

# path to filled mask2 (larger ventricles), relative to this file
FILLED_MASK2 = os.path.join(os.path.dirname(__file__), 'MJ', 'filled_masks2.nii.gz')
# Want the following mapping: TV=1, FH=2, OH=3, TH=4, FL=5, POL=6, TL=7
# Flipped ones have +10 (i.e. FH=12 on right)
# frontal_L,frontal_R,parietal_occipital_L,parietal_occipital_R,temporal_L,temporal_R,sulcal_total,v_frontal_horn_L,v_frontal_horn_R,v_occipital_horn_L,v_occipital_horn_R,v_temporal_horn_L,v_temporal_horn_R,v_third_vent,vent_total,gca_total
REGION_MAPPING = {
    'frontal_L': 5,
    'frontal_R': 15,
    'parietal_occipital_L': 6,
    'parietal_occipital_R': 16,
    'temporal_L': 7,
    'temporal_R': 17,
    'third_vent': 1,
    'v_frontal_horn_L': 2,
    'v_frontal_horn_R': 12,
    'v_occipital_horn_L': 3,
    'v_occipital_horn_R': 13,
    'v_temporal_horn_L': 4,
    'v_temporal_horn_R': 14
}


def get_subset(df, subset=None):
    # filter the dataframe based on the subset
    if subset == 'orchard':
        # get all rows where the ID is a number (numeric)
        df = df[df['ID'].str.isnumeric()]
    elif subset == 'stroke':
        # get all rows where ID starts with a p (case insensitive)
        df = df[df['ID'].str.lower().str.startswith('p')]
    elif subset == 'atrophy':
        # get all rows where the ID starts with sub (case insensitive)
        df = df[df['ID'].str.lower().str.startswith('a')]
    
    return df


def calculate_brain_volume(files, csf_thresh=12):
    '''
    calculate brain volume (GM + WM) for each file in a list of files
    '''
    mask_data = nib.load(FILLED_MASK2).get_fdata()
    # initialize list of CSF volumes
    volumes = [] # each item is a dict of filename, string, and volumes {region: volume}
    n_files = len(files)
    count = 0
    print(f'Calculating brain volume for {n_files} files')
    # loop through files
    for file in files:
        regional_volumes = {
            'filename': file,
            'regions': {},
            'ID': ''
        }
        # update the progress in the terminal, but do it inline (don't print loads of messages on new lines)
        count += 1
        print(f'Processing files %:  {round(count/n_files, 2)*100}', end='\r')
        # load the file
        img = nib.load(file)
        # get the data
        data = img.get_fdata()
        # loop over the regions
        for region, value in REGION_MAPPING.items():
            roi = np.zeros(data.shape)
            # get the volume for this region
            roi[(mask_data == value) & (data <= csf_thresh)] = 1 # use <= to get CSF rather than brain
            volume = np.sum(roi) # in voxels
            # add to the dictionary
            regional_volumes['regions'][region] = volume * img.header['pixdim'][1] * img.header['pixdim'][2] * img.header['pixdim'][3]
        # calculate brain volume. brain is defined as voxels with intensity greater than 12
        brain_volume = np.sum((data <= csf_thresh)) * img.header['pixdim'][1] * img.header['pixdim'][2] * img.header['pixdim'][3]
        regional_volumes['regions']['brain'] = brain_volume
        # get the basename of the file
        basename = os.path.basename(file)
        # get the subject ID (the string before the first underscore)
        sub_id = basename.split('_')[0]
        regional_volumes['ID'] = sub_id
        # add to list
        # volumes.append((sub_id, file, brain_volume))
        volumes.append(regional_volumes)

    return volumes



def save_volume_csv(csv_name, csf_volumes):
    '''
    save volumes as csv
    '''
    # create csv file
    with open(csv_name, 'w') as f:
        # write header
        # f.write('ID,filename,brain_volume\n')
        # write header with a column for each region using a for loop
        header = 'ID,filename,'
        for region in REGION_MAPPING.keys():
            header += f'{region}_volmm3,'
        # add brain volume to the end of the header
        header += 'brain_volume_volmm3'
        header = header + '\n'
        f.write(header)
        # write each row
        for item in csf_volumes:
            # write each row as a string
            # remove sub- from the ID if it is there (e.g. sub-01 becomes 01)
            # f.write(f'{item["ID"].replace("sub-","")},{item[1]},{item[2]}\n')
            # write a csv line with a column for each region
            # f.write(f'{item["ID"]},{item["filename"]},{item["regions"]["brain"]},{item["regions"]["frontal_L"]},{item["regions"]["frontal_R"]},{item["regions"]["parietal_occipital_L"]},{item["regions"]["parietal_occipital_R"]},{item["regions"]["temporal_L"]},{item["regions"]["temporal_R"]},{item["regions"]["third_vent"]},{item["regions"]["v_frontal_horn_L"]},{item["regions"]["v_frontal_horn_R"]},{item["regions"]["v_occipital_horn_L"]},{item["regions"]["v_occipital_horn_R"]},{item["regions"]["v_temporal_horn_L"]},{item["regions"]["v_temporal_horn_R"]}\n')
            csv_row = f'{item["ID"]},{item["filename"]},'
            for region in REGION_MAPPING.keys():
                csv_row += f'{item["regions"][region]},'
            # add the brain volume to the end of the row
            csv_row += f'{item["regions"]["brain"]}'
            csv_row = csv_row + '\n'
            f.write(csv_row)

def get_files(datadir):
    '''
    get the list of files in the data directory
    '''
    # find all nifti files (.nii.gz) in the data directory
    print(f'Finding files in {datadir}')
    files = os.listdir(datadir)
    # filter them to only include nifti files
    files = [os.path.join(datadir, file) for file in files if file.endswith('.nii.gz') and not file.startswith('.')]

    return files

def merge_csv_files(master_csv, volume_csv):
    '''
    merge the master csv file with the volume csv file based on ID
    '''
    # load master csv file
    master_df = pd.read_csv(master_csv)
    # load volume csv file
    volume_df = pd.read_csv(volume_csv)
    # drop the filename column
    volume_df = volume_df.drop(columns=['filename'])
    # merge the two dataframes
    merged_df = pd.merge(master_df, volume_df, on='ID')
    # save as csv
    # create path to save csv from csf_csv path
    csv_path = os.path.dirname(volume_csv)
    # create path to save csv
    merged_path = os.path.join(csv_path, 'merged.csv')
    merged_df.to_csv(merged_path, index=False)
    return merged_path

def plot_scatter(args, model_metrics=None):
    '''
    plot scatter plot of brain volume vs GCA score
    '''
    csv_file = args.trainingcsv
    subset = args.subset
    # load csv file
    df = pd.read_csv(csv_file)
    df = get_subset(df, subset=subset)
    subset = 'all' if subset is None else subset
    n = len(df)
    x = 'gca_total' if args.region == 'brain_volume_volmm3' else args.region.split('_volmm3')[0]
    y = args.region
    sns.lmplot(x=x, y=y, data=df, fit_reg=True) 
    plt.xlim(0, 39 if x == 'gca_total' else 3)
    # add title
    plt.title(f'GCA score vs csf {y} ({subset}, N={n})')
    # add axis labels
    plt.xlabel(f'{x} score')
    plt.ylabel(f'csf {y}')
    # add a text annotation to any point that is an outlier
    # get the outlier points as brain_volume > 2 std from the mean
    outliers = df[df[y] > df[y].mean() + 3*df[y].std()]
    # loop through the outliers
    for index, row in outliers.iterrows():
        # get the brain volume and gca score
        brain_volume = row[y]
        gca_score = row[x]
        # add the text annotation
        plt.annotate(row['ID'], (gca_score, brain_volume))
    # add metrics to the plot
    if model_metrics is not None:
        # get the metrics
        mae = model_metrics['mae']
        mse = model_metrics['mse']
        # add the metrics to the plot
        plt.text(0.75, 0.8, f'MAE: {round(mae, 2)}\nMSE: {round(mse, 2)}', transform=plt.gca().transAxes)
    # save figure
    # create path to save the figure from the csv_file path
    csv_path = os.path.dirname(csv_file)
    # create path to save figure
    figure_path = os.path.join(csv_path, f'scatter_{subset}_{args.region}.png')
    plt.savefig(figure_path, bbox_inches='tight', dpi=300)



def preprocess(args):
    '''
    preprocess the data
    '''
    print('Preprocessing data')
    # get the list of files
    files = get_files(args.datadir)
    # calculate brain volume for each file
    volumes = calculate_brain_volume(files)
    # save volumes as csv
    save_volume_csv(args.csvout, volumes)
    # merge the master csv file with the volume csv file based on ID
    merged_csv = merge_csv_files(args.mastercsv, args.csvout)
    

def train(args):
    '''
    train the model

    subset: orchard, stroke, atrophy, None
    '''
    print('Training model')  
    # load the csv file
    df = pd.read_csv(args.trainingcsv)
    df = get_subset(df, subset=args.subset)
    print(f'Subset: {args.subset}')
    # get the X and y values
    X = df[args.region].values.reshape(-1, 1)
    score_col = 'gca_total' if args.region == 'brain_volume_volmm3' else args.region.split('_volmm3')[0]
    y = df[score_col].values.reshape(-1, 1)
    # test train split 80/20
    X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.2)
    # create the model
    model = LinearRegression()
    # fit the model
    model.fit(X_train, y_train)
    # predict the test data
    y_pred = model.predict(X_test)
    # print metrics
    print(f'Mean absolute error: {metrics.mean_absolute_error(y_test, y_pred)}')
    print(f'Mean squared error: {metrics.mean_squared_error(y_test, y_pred)}')
    print(f'Root mean squared error: {np.sqrt(metrics.mean_squared_error(y_test, y_pred))}')
    linear_model_metrics = {
        'mae': metrics.mean_absolute_error(y_test, y_pred),
        'mse': metrics.mean_squared_error(y_test, y_pred),
    }
    # plot scatter plot of volume vs GCA score
    plot_scatter(args, model_metrics=linear_model_metrics)


def infer():
    pass

def main():
    '''
    run the main program
    '''
    # the first argument is the subcommand. It can be one of the following:
    # preprocess, train, infer
    # call like this:
    # fslpython main.py preprocess --datadir /Volumes/PortableSSD/data/gca/all_normalised_gca_data --csvout ./brain_volumes.csv --mastercsv ./master_file.csv --subset atrophy
    # fslpython main.py train --trainingcsv merged.csv --subset orchard
    parser = argparse.ArgumentParser(description='GCA score inference')
    parser.add_argument('subcommand', type=str, help='subcommand to run')
    parser.add_argument('--datadir', type=str, help='directory with data')
    parser.add_argument('--csvout', type=str, help='output csv file for use with preprocessing')
    parser.add_argument('--mastercsv', type=str, help='master csv file with GCA scores')
    parser.add_argument('--trainingcsv', type=str, help='csv file with training data')
    parser.add_argument('--modelout', type=str, help='output model file')
    parser.add_argument('--infercsv', type=str, help='csv file with inference data')
    parser.add_argument('--model', type=str, help='model file to load')
    parser.add_argument('--subset', type=str, default=None, help='subset of data to use (orchard, stroke, atrophy)')
    # add an argument for the region to use in training and plotting
    parser.add_argument('--region', type=str, default='brain_volume_volmm3', help='region to use in training and plotting')
    args = parser.parse_args()

    if args.subcommand == 'preprocess':
        preprocess(args)
    elif args.subcommand == 'train':
        train(args)
    elif args.subcommand == 'infer':
        infer(args)
    else:
        print('Unknown subcommand')
        parser.print_help()

if __name__ == '__main__':
    main()